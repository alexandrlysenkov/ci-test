//
//  CI_Test_UITests.swift
//  CI Test UITests
//
//  Created by Oleksandr Lysenkov on 3/6/18.
//  Copyright © 2018 Oleksandr Lysenkov. All rights reserved.
//

import XCTest
import SimulatorStatusMagic

class CI_Test_UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.

        SDStatusBarManager.sharedInstance().enableOverrides()
        SDStatusBarManager.sharedInstance().timeString = "9.41"
        SDStatusBarManager.sharedInstance().carrierName = "EASYCORE"

        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        SDStatusBarManager.sharedInstance().disableOverrides()

        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        XCUIApplication().staticTexts["CI Test - Fastlane"].tap()
        
        snapshot("01LoginScreen")
    }
    
}
