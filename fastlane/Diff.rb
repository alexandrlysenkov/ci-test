# Please note, that you have to install this gem before using this file (sudo gem install chunky_png)
require 'chunky_png'
require 'fileutils'
require 'pathname'
include ChunkyPNG::Color

ROW_TEMPLATE = './screenshots-row.erb'
TABLE_TEMPLATE = './screenshots-table.erb'
HTML_TEMPLATE = './screenshots.erb'
  

def relativePath(fromPath, toPath)
  fromPathDirname = File.dirname(fromPath)
  fromPathObj = Pathname.new fromPathDirname
  toPathObj = Pathname.new toPath
  relativeToPath = toPathObj.relative_path_from fromPathObj

  return relativeToPath.to_s
end

def diffScreenshots(screenshotDir, screenshotGoldenMasterDir, screenshotDiffDir, diffHTMLfile, languages)
  comparedScreenshotBlocks = diffFiles(screenshotDir, screenshotGoldenMasterDir, screenshotDiffDir, languages)

  puts ""
  puts ""

  # render rows
  tablesResult = ""
  differencePercentage = 0
  
  comparedScreenshotBlocks.each do |comparedScreenshotBlock|
    rowsHTML = ""
    @LANGUAGE = comparedScreenshotBlock[:language]

    comparedScreenshotBlock[:diffs].each do |comparedScreenshot|

      @DEVICE = comparedScreenshot[:fileName]

      @SCREENSHOT = relativePath(diffHTMLfile, comparedScreenshot[:file])
      @SCREENSHOT_GOLDEN = relativePath(diffHTMLfile, comparedScreenshot[:masterFile])
      @SCREENSHOT_DIFF = relativePath(diffHTMLfile, comparedScreenshot[:diffFile])

      row = File.read(ROW_TEMPLATE)
      rowResult = ERB.new(row).result(binding)  

      rowsHTML = rowsHTML + rowResult

      differencePercentage = differencePercentage + comparedScreenshot[:difference]
    end

    # render table
    @ROWS = rowsHTML
    
    table = File.read(TABLE_TEMPLATE)
    tablesResult = tablesResult + ERB.new(table).result(binding)
  end

  # render template
  @TABLE = tablesResult
  
  template = File.read(HTML_TEMPLATE)
  result = ERB.new(template).result(binding)

  # write result to file
  File.open(diffHTMLfile, 'w+') do |f|
    f.write result
  end

  return differencePercentage == 0
end

def diffFiles(screenshotDir, screenshotGoldenMasterDir, screenshotDiffDir, languages)
  comparedScreenshots = Array.new

  languages.each do |language|
    # get all the files from screenshotDir
    files = Dir.entries(screenshotDir + "#{language}/").select {|f| !File.directory? f}

    puts ""
    puts ""
    puts "Language: #{language}"

    differenceReport = {
      :language => "#{language}",
      :diffs => Array.new
    }

    files.each do |file|
      screenshotFile = screenshotDir + "#{language}/" + file
      screenshotMasterFile = screenshotGoldenMasterDir + "#{language}/" + file
      screenshotDiffFile = screenshotDiffDir + "#{language}/" + file

      if File.file?(screenshotFile) && File.file?(screenshotMasterFile)
        puts ""
        puts "file: #{screenshotFile}"
        
        difference = diffFile(screenshotFile, screenshotMasterFile, screenshotDiffFile)
        
        puts "difference: #{difference}"
        
        differenceReport[:diffs].push({
          :fileName => file,
          :file => screenshotFile,
          :masterFile => screenshotMasterFile,
          :diffFile => screenshotDiffFile,
          :difference => difference
        })
      end
    end
    
    comparedScreenshots.push(differenceReport)
  end

  return comparedScreenshots
end


def diffFile(screenshot, screenshotGoldenMaster, screenshotDiff) 

  images = [
    ChunkyPNG::Image.from_file(screenshot),
    ChunkyPNG::Image.from_file(screenshotGoldenMaster)
  ]

  output = ChunkyPNG::Image.new(images.first.width, images.first.height, WHITE)

  diff = []

  images.first.height.times do |y|
      images.first.row(y).each_with_index do |pixel, x|
        unless pixel == images.last[x,y]
          score = Math.sqrt(
            (r(images.last[x,y]) - r(pixel)) ** 2 +
            (g(images.last[x,y]) - g(pixel)) ** 2 +
            (b(images.last[x,y]) - b(pixel)) ** 2
          ) / Math.sqrt(MAX ** 2 * 3)

          output[x,y] = grayscale(MAX - (score * MAX).round)
          diff << score
        end
      end
  end

  dirname = File.dirname(screenshotDiff)
  unless File.directory?(dirname)
    FileUtils.mkdir_p(dirname)
  end

  if File.file?(screenshotDiff)
    File.delete(screenshotDiff)
  end
  output.save(screenshotDiff)

  difference = 0
  
  if images.first.pixels.length > 0
    difference = (diff.inject {|sum, value| sum + value} / images.first.pixels.length) * 100
  end

  return difference
end